use rand::seq::SliceRandom;
use rand::thread_rng;
use std::env;
use std::time::Instant;

fn main() {
    let now = Instant::now();
    let args: Vec<String> = env::args().collect();
    let anzahl_spiele: u32 = if args.len() > 1 {
        match args[1].parse::<u32>() {
            Ok(result) => result,
            Err(_error) => {
                println!("Fehler beim Parsen des Kommandozeilenarguments (\"{}\"). Daher wird der Standardwert 100 für die Spielanzahl verwendet.",_error);
                100
            }
        }
    } else {
        println!("Es wurden keine Kommandozeilenargumente übergeben. Daher wird der Standardwert 100 für die Spielanzahl verwendet.");                
        100
    };
    let bin_size = 100;
    let limit_runden = 100000;

    let mut rundenzahlen = vec![];
    for _ in 0..anzahl_spiele {
        rundenzahlen.push(bataille_royale(limit_runden));
    }
    rundenzahlen.sort();
    println!(
        "Bei {} Spielen Leben oder Tod ergeben sich folgende Ergebnisse:",
        anzahl_spiele
    );
    if let Some(max_rundenzahlen) = rundenzahlen.iter().max() {
        println!(
            "Maximale Rundenzahl in den Spielen = {} mit Limit = {}",
            max_rundenzahlen, limit_runden
        );
        let mut left_bin: u32;
        let mut right_bin: u32 = 0;
        while right_bin < *max_rundenzahlen {
            left_bin = right_bin;
            right_bin += bin_size;
            println!(
                "Rundenzahlen in ({},{}]: {}",
                left_bin,
                right_bin,
                rundenzahlen
                    .iter()
                    .filter(|&x| *x > left_bin && *x <= right_bin)
                    .count()
            );
        }
    }
    let gesamtlaufzeit = now.elapsed().as_secs();
    println!(
        "Programmlaufzeit gesamt = {}s, pro Spiel = {}ms",
        gesamtlaufzeit,
        1000.0 * (gesamtlaufzeit as f64) / (anzahl_spiele as f64)
    );
}

fn bataille_royale(limit_runden: u32) -> u32 {
    let anzahl_farben = 4;
    let anzahl_karten_je_farbe = 8;
    let anzahl_karten_je_spieler: usize = (anzahl_farben * anzahl_karten_je_farbe) / 2;
    let mut runden = 0;
    let mut karten: Vec<u32> = vec![];
    for i in 0..anzahl_karten_je_farbe {
        for _ in 0..anzahl_farben {
            karten.push(i as u32);
        }
    }
    karten.shuffle(&mut thread_rng());
    let mut rot = karten.split_off(anzahl_karten_je_spieler);
    let mut blau = karten;
    let mut stack = vec![];
    loop {
        match (rot.pop(), blau.pop()) {
            (Some(rote_karte), Some(blaue_karte)) => {
                stack.insert(0, rote_karte);
                stack.insert(0, blaue_karte);
                if rote_karte > blaue_karte {
                    //rot gewinnt
                    stack.shuffle(&mut thread_rng());
                    rot.splice(0..0, stack);
                    stack = vec![];
                } else if blaue_karte > rote_karte {
                    //blau gewinnt
                    stack.shuffle(&mut thread_rng());
                    blau.splice(0..0, stack);
                    stack = vec![];
                } else {
                    //stechen mit extra Karte
                    match (rot.pop(), blau.pop()) {
                        (Some(rote_karte_extra), Some(blaue_karte_extra)) => {
                            stack.insert(0, rote_karte_extra);
                            stack.insert(0, blaue_karte_extra);
                        }
                        _ => {
                            break;
                        }
                    }
                }
            }
            _ => {
                //Spielende, da ein Spieler keine Karten mehr hat
                break;
            }
        }
        runden += 1;
        if runden > limit_runden {
            break;
        }
    }
    return runden;
}
